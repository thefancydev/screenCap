﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScapLIB;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using System.Timers;
using System.Windows.Forms;

namespace ScreenCap
{
    class Program
    {
        private static string saveDir;
        public static void screenShot(object source, ElapsedEventArgs e)
        {
            int imageCounter = 0;

            imageCounter++;

            var bounds = Screen.PrimaryScreen.Bounds;
            using (var bmp = new Bitmap(bounds.Width,
                                        bounds.Height,
                                        PixelFormat.Format32bppArgb))
            using (var gfx = Graphics.FromImage(bmp))
            {
                gfx.CopyFromScreen(bounds.X,
                                   bounds.Y,
                                   0,
                                   0,
                                   bounds.Size,
                                   CopyPixelOperation.SourceCopy);
                bmp.Save(saveDir + "\\" + imageCounter + "shot.png");
                Console.WriteLine($"Image taken! Can be found at {saveDir + "\\" + imageCounter + "shot.png"}");
            }
        }
        static void Main(string[] args)
        {
            if (args.Length != 2)
                Console.WriteLine("Usage: sca (interval in seconds) '(save directory)'");
            else
            {
                try
                {
                    int.Parse(args[0]);
                    Path.GetDirectoryName(args[1]);
                }
                catch (Exception)
                {
                    Environment.Exit(0);
                }
                Console.WriteLine("Press Q to quit");
                var ts = new System.Timers.Timer(int.Parse(args[0])*1000);
                ts.Elapsed += screenShot;
                ts.Start();
                while (Console.Read() != 'Q') ;

                Environment.Exit(0);
            }
        }

    }
}
